
from flask import Flask
from flask import render_template, request, redirect, session
from flaskext.mysql import MySQL

app=Flask(__name__)
app.secret_key="hola"
#Establecemos la conexión a la base de datos
mysql=MySQL()
app.config['MYSQL_DATABASE_HOST']='localhost'
app.config['MYSQL_DATABASE_USER']='root'
app.config['MYSQL_DATABASE_PASSWORD']=''
app.config['MYSQL_DATABASE_DB']='libreria'
mysql.init_app(app)

@app.route('/')
def inicio():
    return render_template('sitio/index.html')

@app.route('/libros')
def libros():
    return render_template('sitio/libros.html')

@app.route('/admin/')
def admin_libros():
    if not 'login' in session:
        return redirect("/")

    conexion=mysql.connect()
    #Traer la información de la base de datos
    cursor=conexion.cursor()
    cursor.execute("SELECT * FROM libros")
    libros=cursor.fetchall()
    conexion.commit()

    return render_template('admin/libros.html', libros=libros)

@app.route('/admin/libros/guardar', methods=['POST'])
def admin_libros_guardar():
    __nombre = request.form['nombre_libro']
    __imagen = request.files['imagen_libro']
    __url = request.form['url_libro']
    #print(__nombre)
    #print(__imagen)
    #print(__url)
    #Ejecutar la sentencia de inserción de datos a la tabla
    sql="INSERT INTO libros (nombre, imagen, url) VALUES(%s, %s,%s)"
    datos=(__nombre, __imagen.filename, __url)
    conexion=mysql.connect()
    cursor=conexion.cursor()
    cursor.execute(sql, datos)
    conexion.commit()
    return redirect('/admin/')

@app.route('/admin/libros/editar', methods=['POST'])
def editar():
    __id__=request.form['id']
    conexion = mysql.connect()
    cursor =conexion.cursor()
    consulta = "SELECT * FROM libros WHERE id=%s"
    cursor.execute(consulta, __id__)
    dato=cursor.fetchall()
    conexion.commit()
    return render_template('admin/editar.html', d=dato[0])

@app.route('/admin/libros/modificar', methods=['POST'])
def modificar():
    __id__ = request.form['filtro']
    __nombre__ = request.form['nombre_libro']
    __url__ = request.form['url_libro']
    conexion = mysql.connect()
    cursor =conexion.cursor()
    cursor.execute("UPDATE libros SET nombre=%s, url=%s WHERE id=%s",(__nombre__, __url__, __id__))
    conexion.commit()
    return redirect('/admin/')

@app.route('/admin/libros/eliminar', methods=['POST'])
def eliminar():
    __id__ = request.form['id']
    conexion=mysql.connect()
    cursor=conexion.cursor()
    cursor.execute("DELETE FROM libros WHERE id=%s",(__id__))
    conexion.commit() 
    return redirect('/admin/')

@app.route('/admin/login')
def admin_login():
    return render_template('admin/login.html')

@app.route('/admin/login', methods=['POST'])
def admin_login_post():
    __usuario = request.form['txtUsuario']
    __password = request.form['txtPassword']
    #print(__usuario)
    #print(__password)
    if __usuario=="admin" and __password=="123":
        session["login"]=True
        session["usuario"]="Edisson"
        return redirect("/admin/")

    return render_template('admin/login.html')

@app.route('/cerrar/')
def admin__login_cerrar():
    session.clear()
    return redirect('/admin/login')

if __name__=='__main__':
    app.run(debug=True)